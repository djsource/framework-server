package cn.backflow.admin.common.secure.exceptions;

public class PermissionDeniedException extends RuntimeException {
    public PermissionDeniedException(String msg) {
        super(msg);
    }
}
